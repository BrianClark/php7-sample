FROM steadyserv/php-apache:7.1

# if not "true", composer install will be skipped (useful for local dev)
ARG COMPOSER_INSTALL="true"
# additional composer args supplied at build time
ARG COMPOSER_INSTALL_ARGS=""

COPY config/php.ini ${PHP_INI_DIR}/php.ini

COPY composer.* /var/www/html/

RUN if [ "$COMPOSER_INSTALL" = "true" ]; then composer install $COMPOSER_INSTALL_ARGS; fi

COPY src /var/www/html/
